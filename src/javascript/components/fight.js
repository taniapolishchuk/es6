export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
    const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');

    const initialFirstFigherHealth = firstFighter.get('health');
    const intialSecondFighterHealth = secondFighter.get('health');

    let pressed = new Set();

    const firstFighterCriticalAttackDebounce = debounce(function () {
      return criticalAttack(firstFighter, secondFighter, intialSecondFighterHealth, secondFighterHealthIndicator);
    }, 10000);
    const secondFighterCriticalAttackDebounce = debounce(function () {
      return criticalAttack(secondFighter, firstFighter, initialFirstFigherHealth, firstFighterHealthIndicator);
    }, 10000);

    document.addEventListener('keydown', (event) => {
      pressed.add(event.code);
      if (pressed.has('KeyA')) {
        let damage = getDamage(firstFighter, secondFighter);

        if (pressed.has('KeyL')) {
          damage = 0;
        }

        deductDamageFromHealth(secondFighter, damage, intialSecondFighterHealth, secondFighterHealthIndicator);
        checkHealth(secondFighter, firstFighter, resolve, secondFighterHealthIndicator);
      }

      if (pressed.has('KeyJ')) {
        let damage = getDamage(secondFighter, firstFighter);

        if (pressed.has('KeyD')) {
          damage = 0;
        }

        deductDamageFromHealth(firstFighter, damage, initialFirstFigherHealth, firstFighterHealthIndicator);
        checkHealth(firstFighter, secondFighter, resolve, firstFighterHealthIndicator);
      }

      if (firstFighterCriticalCombination(pressed)) {
        firstFighterCriticalAttackDebounce();
        checkHealth(secondFighter, firstFighter, resolve, secondFighterHealthIndicator);
      }

      if (secondFighterCriticalCombination(pressed)) {
        secondFighterCriticalAttackDebounce();
        checkHealth(firstFighter, secondFighter, resolve, firstFighterHealthIndicator);
      }
    });

    document.addEventListener('keyup', function (event) {
      pressed.delete(event.code);
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.get('attack') * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = fighter.get('defense') * dodgeChance;

  return power;
}

export function deductDamageFromHealth(fighter, damage, initialFighterHealth, fighterHealthIndicator) {
    const newHealth = fighter.get('health') - damage;
    fighter.set('health', newHealth);
    const healthIndicatorWidth = (fighter.get('health') / initialFighterHealth) * 100 + '%';
    fighterHealthIndicator.style.width = healthIndicatorWidth;
}

function debounce(f, ms) {
  let isCooldown = false;

  return function () {
    if (isCooldown) return;

    f.apply(this, arguments);

    isCooldown = true;

    setTimeout(() => (isCooldown = false), ms);
  };
}

export function criticalAttack(attacker, defenser, initialDefenserHealth, defenserHealthIndicator) {
  let damage = attacker.get('attack') * 2;
  const newHealth = defenser.get('health') - damage;
  defenser.set('health', newHealth);
  const healthIndicatorWidth = (defenser.get('health') / initialDefenserHealth) * 100 + '%';
  defenserHealthIndicator.style.width = healthIndicatorWidth;
}

export function checkHealth(defenser, attacker, resolve, defenserHealthIndicator) {
  if (defenser.get('health') <= 0) {
    defenserHealthIndicator.style.width = '0';
    resolve(attacker);
  }
}

export function firstFighterCriticalCombination(pressed) {
  if (pressed.has('KeyQ') && pressed.has('KeyW') && pressed.has('KeyE')) {
    return true;
  }
  return false;
}

export function secondFighterCriticalCombination(pressed) {
  if (pressed.has('KeyU') && pressed.has('KeyI') && pressed.has('KeyO')) {
    return true;
  }
  return false;
}
