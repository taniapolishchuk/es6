import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const name = fighter.get('name');
  const health = fighter.get('health');
  const attack = fighter.get('attack');
  const defense = fighter.get('defense');

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const imageElement = createFighterImage(fighter);

  const nameElement = createElement({
    tagName: 'p',
    className: 'fighter-preview__name',
  });
  nameElement.innerText = name;

  const healthElement = createElement({
    tagName: 'p',
    className: 'fighter-preview__health',
  });
  healthElement.innerText = `Health: ${health}`;

  const attackElement = createElement({
    tagName: 'p',
    className: 'fighter-preview__attack',
  });
  attackElement.innerText = `Attack: ${attack}`;

  const defenseElement = createElement({
    tagName: 'p',
    className: 'fighter-preview__defense',
  });
  defenseElement.innerText = `Defense: ${defense}`;

  fighterElement.append(imageElement, nameElement, healthElement, attackElement, defenseElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const source = fighter.get('source');
  const name = fighter.get('name');
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
