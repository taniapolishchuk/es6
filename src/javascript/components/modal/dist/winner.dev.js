"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showWinnerModal = showWinnerModal;

var _modal = require("./modal");

var _domHelper = require("../../helpers/domHelper");

function showWinnerModal(fighter) {
  var name = fighter.get('name');
  var bodyElement = (0, _domHelper.createElement)({
    tagName: 'p',
    className: 'modal-body'
  });

  var onClose = function onClose() {
    location.reload();
    return false;
  };

  bodyElement.innerText = name;
  (0, _modal.showModal)({
    title: 'Winner',
    bodyElement: bodyElement,
    onClose: onClose
  });
}