import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const name = fighter.get('name');
  const bodyElement = createElement({
    tagName: 'p',
    className: 'modal-body',
  });

  const onClose = () => {
    location.reload();
    return false;
  };

  bodyElement.innerText = name;
  showModal({ title: 'Winner', bodyElement, onClose });
}
