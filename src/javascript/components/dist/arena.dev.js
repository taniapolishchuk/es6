"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderArena = renderArena;

var _domHelper = require("../helpers/domHelper");

var _fighterPreview = require("./fighterPreview");

var _fight = require("./fight");

var _winner = require("./modal/winner");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function renderArena(selectedFighters) {
  var root = document.getElementById('root');
  var arena = createArena(selectedFighters);
  root.innerHTML = '';
  root.append(arena);

  var _selectedFighters = _slicedToArray(selectedFighters, 2),
      firstFighter = _selectedFighters[0],
      secondFighter = _selectedFighters[1];

  (0, _fight.fight)(firstFighter, secondFighter).then(function (fighter) {
    return (0, _winner.showWinnerModal)(fighter);
  });
}

function createArena(selectedFighters) {
  var arena = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___root'
  });
  var healthIndicators = createHealthIndicators.apply(void 0, _toConsumableArray(selectedFighters));
  var fighters = createFighters.apply(void 0, _toConsumableArray(selectedFighters));
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  var healthIndicators = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___fight-status'
  });
  var versusSign = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___versus-sign'
  });
  var leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  var rightFighterIndicator = createHealthIndicator(rightFighter, 'right');
  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  var name = fighter.get('name');
  var container = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___fighter-indicator'
  });
  var fighterName = (0, _domHelper.createElement)({
    tagName: 'span',
    className: 'arena___fighter-name'
  });
  var indicator = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___health-indicator'
  });
  var bar = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: {
      id: "".concat(position, "-fighter-indicator")
    }
  });
  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);
  return container;
}

function createFighters(firstFighter, secondFighter) {
  var battleField = (0, _domHelper.createElement)({
    tagName: 'div',
    className: "arena___battlefield"
  });
  var firstFighterElement = createFighter(firstFighter, 'left');
  var secondFighterElement = createFighter(secondFighter, 'right');
  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  var imgElement = (0, _fighterPreview.createFighterImage)(fighter);
  var positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  var fighterElement = (0, _domHelper.createElement)({
    tagName: 'div',
    className: "arena___fighter ".concat(positionClassName)
  });
  fighterElement.append(imgElement);
  return fighterElement;
}