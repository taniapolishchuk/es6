"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFighters = createFighters;

var _domHelper = require("../helpers/domHelper");

var _fighterSelector = require("./fighterSelector");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function createFighters(fighters) {
  var selectFighter = (0, _fighterSelector.createFightersSelector)();
  var container = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'fighters___root'
  });
  var preview = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'preview-container___root'
  });
  var fightersList = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'fighters___list'
  });
  var fighterElements = fighters.map(function (fighter) {
    return createFighter(fighter, selectFighter);
  });
  fightersList.append.apply(fightersList, _toConsumableArray(fighterElements));
  container.append(preview, fightersList);
  return container;
}

function createFighter(fighter, selectFighter) {
  var fighterElement = (0, _domHelper.createElement)({
    tagName: 'div',
    className: 'fighters___fighter'
  });
  var imageElement = createImage(fighter);

  var onClick = function onClick(event) {
    return selectFighter(event, fighter._id);
  };

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);
  return fighterElement;
}

function createImage(fighter) {
  var source = fighter.source,
      name = fighter.name;
  var attributes = {
    src: source,
    title: name,
    alt: name
  };
  var imgElement = (0, _domHelper.createElement)({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes: attributes
  });
  return imgElement;
}