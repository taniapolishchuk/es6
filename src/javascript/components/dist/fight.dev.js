"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fight = fight;
exports.getDamage = getDamage;
exports.getHitPower = getHitPower;
exports.getBlockPower = getBlockPower;
exports.deductDamageFromHealth = deductDamageFromHealth;
exports.criticalAttack = criticalAttack;
exports.checkHealth = checkHealth;
exports.firstFighterCriticalCombination = firstFighterCriticalCombination;
exports.secondFighterCriticalCombination = secondFighterCriticalCombination;

function fight(firstFighter, secondFighter) {
  return regeneratorRuntime.async(function fight$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          return _context.abrupt("return", new Promise(function (resolve) {
            var firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
            var secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');
            var initialFirstFigherHealth = firstFighter.get('health');
            var intialSecondFighterHealth = secondFighter.get('health');
            var pressed = new Set();
            var firstFighterCriticalAttackDebounce = debounce(function () {
              return criticalAttack(firstFighter, secondFighter, intialSecondFighterHealth, secondFighterHealthIndicator);
            }, 10000);
            var secondFighterCriticalAttackDebounce = debounce(function () {
              return criticalAttack(secondFighter, firstFighter, initialFirstFigherHealth, firstFighterHealthIndicator);
            }, 10000);
            document.addEventListener('keydown', function (event) {
              pressed.add(event.code);

              if (pressed.has('KeyA')) {
                var damage = getDamage(firstFighter, secondFighter);

                if (pressed.has('KeyL')) {
                  damage = 0;
                }

                deductDamageFromHealth(secondFighter, damage, intialSecondFighterHealth, secondFighterHealthIndicator);
                checkHealth(secondFighter, firstFighter, resolve, secondFighterHealthIndicator);
              }

              if (pressed.has('KeyJ')) {
                var _damage = getDamage(secondFighter, firstFighter);

                if (pressed.has('KeyD')) {
                  _damage = 0;
                }

                deductDamageFromHealth(firstFighter, _damage, initialFirstFigherHealth, firstFighterHealthIndicator);
                checkHealth(firstFighter, secondFighter, resolve, firstFighterHealthIndicator);
              }

              if (firstFighterCriticalCombination(pressed)) {
                firstFighterCriticalAttackDebounce();
                checkHealth(secondFighter, firstFighter, resolve, secondFighterHealthIndicator);
              }

              if (secondFighterCriticalCombination(pressed)) {
                secondFighterCriticalAttackDebounce();
                checkHealth(firstFighter, secondFighter, resolve, firstFighterHealthIndicator);
              }
            });
            document.addEventListener('keyup', function (event) {
              pressed["delete"](event.code);
            });
          }));

        case 1:
        case "end":
          return _context.stop();
      }
    }
  });
}

function getDamage(attacker, defender) {
  // return damage
  var damage = getHitPower(attacker) - getBlockPower(defender);
  return damage;
}

function getHitPower(fighter) {
  // return hit power
  var criticalHitChance = Math.random() + 1;
  var power = fighter.get('attack') * criticalHitChance;
  return power;
}

function getBlockPower(fighter) {
  // return block power
  var dodgeChance = Math.random() + 1;
  var power = fighter.get('defense') * dodgeChance;
  return power;
}

function deductDamageFromHealth(fighter, damage, initialFighterHealth, fighterHealthIndicator) {
  if (damage > 0) {
    var newHealth = fighter.get('health') - damage;
    fighter.set('health', newHealth);
    var healthIndicatorWidth = fighter.get('health') / initialFighterHealth * 100 + '%';
    fighterHealthIndicator.style.width = healthIndicatorWidth;
  }
}

function debounce(f, ms) {
  var isCooldown = false;
  return function () {
    if (isCooldown) return;
    f.apply(this, arguments);
    isCooldown = true;
    setTimeout(function () {
      return isCooldown = false;
    }, ms);
  };
}

function criticalAttack(attacker, defenser, initialDefenserHealth, defenserHealthIndicator) {
  var damage = attacker.get('attack') * 2;
  var newHealth = defenser.get('health') - damage;
  defenser.set('health', newHealth);
  var healthIndicatorWidth = defenser.get('health') / initialDefenserHealth * 100 + '%';
  defenserHealthIndicator.style.width = healthIndicatorWidth;
}

function checkHealth(defenser, attacker, resolve, defenserHealthIndicator) {
  if (defenser.get('health') <= 0) {
    defenserHealthIndicator.style.width = '0';
    resolve(attacker);
  }
}

function firstFighterCriticalCombination(pressed) {
  if (pressed.has('KeyQ') && pressed.has('KeyW') && pressed.has('KeyE')) {
    return true;
  }

  return false;
}

function secondFighterCriticalCombination(pressed) {
  if (pressed.has('KeyU') && pressed.has('KeyI') && pressed.has('KeyO')) {
    return true;
  }

  return false;
}