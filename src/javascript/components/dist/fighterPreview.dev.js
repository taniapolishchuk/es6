"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFighterPreview = createFighterPreview;
exports.createFighterImage = createFighterImage;

var _domHelper = require("../helpers/domHelper");

function createFighterPreview(fighter, position) {
  var name = fighter.get('name');
  var health = fighter.get('health');
  var attack = fighter.get('attack');
  var defense = fighter.get('defense');
  var positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  var fighterElement = (0, _domHelper.createElement)({
    tagName: 'div',
    className: "fighter-preview___root ".concat(positionClassName)
  });
  var imageElement = createFighterImage(fighter);
  var nameElement = (0, _domHelper.createElement)({
    tagName: 'p',
    className: 'fighter-preview__name'
  });
  nameElement.innerText = name;
  var healthElement = (0, _domHelper.createElement)({
    tagName: 'p',
    className: 'fighter-preview__health'
  });
  healthElement.innerText = "Health: ".concat(health);
  var attackElement = (0, _domHelper.createElement)({
    tagName: 'p',
    className: 'fighter-preview__attack'
  });
  attackElement.innerText = "Attack: ".concat(attack);
  var defenseElement = (0, _domHelper.createElement)({
    tagName: 'p',
    className: 'fighter-preview__defense'
  });
  defenseElement.innerText = "Defense: ".concat(defense);
  fighterElement.append(imageElement, nameElement, healthElement, attackElement, defenseElement);
  return fighterElement;
}

function createFighterImage(fighter) {
  var source = fighter.get('source');
  var name = fighter.get('name');
  var attributes = {
    src: source,
    title: name,
    alt: name
  };
  var imgElement = (0, _domHelper.createElement)({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes: attributes
  });
  return imgElement;
}